;
; Installer script for ABackup installation
; $VER: Install_ABackup v36.2 (3.9.95)
;
;********* Initialisations ****************************************************
;
(set MainDir            "")
(set PrefDir            "")
(set DefPrefDir         "SYS:Prefs")
(set SourceDir          "/")
(set XPKSourceDir       (tackon SourceDir "libs/compressors"))
(set TmpFile            (cat "T:RunABPrefs_" (database "total-mem")))
;
;********* Setup all the messages *********************************************
;
(procedure SetupMessages (
  (set DefHelpDir       (tackon "HELP:" @language))
  (set DefCatalogDir    (tackon "LOCALE:Catalogs" @language))

  (if (= @language "english")
    (
      (set #InstallWhat         "What do you want to install ?")
      (set #InstallChoice1      "Main program")
      (set #InstallChoice2      "Preferences program")
      (set #InstallChoice3      "Online help")
      (set #InstallChoice4      "XPK libraries")
      (set #InstallChoice5      "")
      (set #SelectMainDir       "Select the directory where to copy the Main program.")
      (set #CopyingMainProg     "Copying the Main program...")
      (set #SelectPrefDir       "Select the directory where to copy the Prefs program.")
      (set #BadPrefDir          "Sorry, the Main program and the Prefs program can't be installed in the same directory !")
      (set #CopyingPrefProg     "Copying the Prefs program...")
      (set #UpdateMainTT1       "Updating the %s tooltype of the Main program...")
      (set #UpdateMainTT2       "Don't forget to update the %s tooltype of the Main program.")
      (set #UpdateMainTTH1      "This will allow the Main program to call the Prefs program.")
      (set #UpdateMainTTH2      "This will allow the Main program to find the help file.")
      (set #UpdatePrefTT1       "Updating the %s tooltype of the Prefs program...")
      (set #UpdatePrefTT2       "Don't forget to update the %s tooltype of the Prefs program.")
      (set #UpdatePrefTTH2      "This will allow the Prefs program to find the help file.")
      (set #SelectHelpDir       "Select the directory where to copy online help.")
      (set #CopyingHelp         "Copying online help...")
      (set #SelectXPKDir        "Select the directory where to copy XPK libraries.")
      (set #CopyingXPK          "Copying XPK libraries...")
      (set #SelectCatalogDir    "Select the directory where to copy the catalog file.")
      (set #CopyingCatalog      "Copying catalog file...")
      (set #RunPrefProg         "Run the Pref program.")
      (set #RunPrefHelp         "This will allow you to setup the ABackup Preferences.")
      (set #Shareware1          "ABackup is a SHAREWARE program !")
      (set #Shareware2          "If you like it and use it, a contribution of US $20 is asked.")
      (set #Shareware3          "Please read the 'Diffusion' section of the documentation, for more details")
    )
  )
  (if (= @language "fran�ais")
    (
      (set #InstallWhat         "Que voulez-vous installer ?")
      (set #InstallChoice1      "Programme principal")
      (set #InstallChoice2      "Programme de param�trage")
      (set #InstallChoice3      "Aide en ligne")
      (set #InstallChoice4      "Biblioth�ques XPK")
      (set #InstallChoice5      "Catalogue")
      (set #SelectMainDir       "Indiquez dans quel r�pertoire installer le programme principal.")
      (set #CopyingMainProg     "Copie du programme principal...")
      (set #SelectPrefDir       "Indiquez dans quel r�pertoire installer le programme de param�trage.")
      (set #BadPrefDir          "D�sol�, le programme principal et le programme de param�trage ne peuvent �tre install�s dans le m�me r�pertoire !")
      (set #CopyingPrefProg     "Copie du programme de param�trage...")
      (set #UpdateMainTT1       "Mise � jour du type d'outil %s du programme principal...")
      (set #UpdateMainTT2       "N'oubliez pas de mettre � jour le type d'outil %s du programme principal.")
      (set #UpdateMainTTH1      "Ceci permettra au programme principal d'appeller le programme de param�trage.")
      (set #UpdateMainTTH2      "Ceci permettra au programme principal de trouver le fichier d'aide.")
      (set #UpdatePrefTT1       "Mise � jour du type d'outil %s du programme de param�trage...")
      (set #UpdatePrefTT2       "N'oubliez pas de mettre � jour le type d'outil %s du programme de param�trage.")
      (set #UpdatePrefTTH2      "Ceci permettra au programme de param�trage de trouver le fichier d'aide.")
      (set #SelectHelpDir       "Indiquez dans quel r�pertoire installer les fichiers d'aide.")
      (set #CopyingHelp         "Copie des fichiers d'aide...")
      (set #SelectXPKDir        "Indiquez dans quel r�pertoire installer les biblioth�ques XPK.")
      (set #CopyingXPK          "Copie des biblioth�ques XPK...")
      (set #SelectCatalogDir    "Indiquez dans quel r�pertoire installer le fichier catalogue.")
      (set #CopyingCatalog      "Copie du fichier catalogue...")
      (set #RunPrefProg         "Appel du programme de param�trage.")
      (set #RunPrefHelp         "Ceci vous permettra de param�trer ABackup.")
      (set #Shareware1          "ABackup est un programme SHAREWARE !")
      (set #Shareware2          "Si vous l'utilisez et l'appr�ciez, une contribution de 100 FF est demand�e.")
      (set #Shareware3          "Reportez-vous au chapitre 'Diffusion' de la documentation, pour plus de d�tails.")
    )
  )
  (if (= @language "deutsch")
    (
      (set #InstallWhat         "Was m�chten Sie installieren ?")
      (set #InstallChoice1      "Hauptprogramm")
      (set #InstallChoice2      "Voreinstellungsprogramm")
      (set #InstallChoice3      "Online Hilfe")
      (set #InstallChoice4      "XPK Libraries")
      (set #InstallChoice5      "Katalog")
      (set #SelectMainDir       "W�hlen Sie das Verzeichnis f�r das Hauptprogramm.")
      (set #CopyingMainProg     "Kopiere das Hauptprogramm...")
      (set #SelectPrefDir       "W�hlen Sie das Verzeichnis f�r das Voreinstellungsprogramm.")
      (set #BadPrefDir          "Sorry, Hauptprogramm und Voreinstellungsprogramm d�rfen nicht in das selbe Verzeichnis !")
      (set #CopyingPrefProg     "Kopiere das Voreinstellungsprogramm...")
      (set #UpdateMainTT1       "Aktualisiere das %s Merkmal des Hauptprogramms...")
      (set #UpdateMainTT2       "Vergessen Sie nicht das %s Merkmal des Hauptprogramms zu aktualisieren.")
      (set #UpdateMainTTH1      "Dies erlaubt dem Hauptprogramm den Aufruf des Voreinstellungsprogramms.")
      (set #UpdateMainTTH2      "Dies erlaubt dem Hauptprogramm die Hilfe-Datei zu finden.")
      (set #UpdatePrefTT1       "Aktualisiere das %s Merkmal des Voreinstellungsprogramms...")
      (set #UpdatePrefTT2       "Vergessen Sie nicht das %s Merkmal des Voreinstellungsprogramms zu aktualisieren.")
      (set #UpdatePrefTTH2      "Dies erlaubt dem Voreinstellungsprogramm die Hilfe-Datei zu finden.")
      (set #SelectHelpDir       "W�hlen Sie das Verzeichnis f�r die Online-Hilfe.")
      (set #CopyingHelp         "Kopiere die Datei f�r die Online-Hilfe...")
      (set #SelectXPKDir        "W�hlen Sie das Verzeichnis f�r die XPK Libraries.")
      (set #CopyingXPK          "Kopiere die XPK Libraries...")
      (set #SelectCatalogDir    "W�hlen Sie das Verzeichnis f�r die Katalogdatei.")
      (set #CopyingCatalog      "Kopiere die Katalogdatei...")
      (set #RunPrefProg         "Starte das Voreinstellungsprogramm.")
      (set #RunPrefHelp         "Dies erlaubt Ihnen die Voreinstellungen von ABackup zu �ndern.")
      (set #Shareware1          "ABackup ist SHAREWARE !")
      (set #Shareware2          "Falls ihnen das Programm gef�llt und Sie es regelm��ig benutzen, entrichten Sie bitte eine Geb�hr von DM 30.")
      (set #Shareware3          "Schlagen Sie bitte im Kapitel 'Diffusion' (Vertrieb) des Handbuchs nach, um mehr Informationen zu erhalten.")
    )
  )
  (if (= @language "italiano")
    (
      (set #InstallWhat         "Che cosa vuoi installare ?")
      (set #InstallChoice1      "Programma Principale")
      (set #InstallChoice2      "Programma Preferenze")
      (set #InstallChoice3      "Aiuto in-linea")
      (set #InstallChoice4      "Librerie XPK")
      (set #InstallChoice5      "Catalogo Italiano")
      (set #SelectMainDir       "Seleziona la directory dove copiare il Programma Principale.")
      (set #CopyingMainProg     "Sto copiando il Programma Principale...")
      (set #SelectPrefDir       "Seleziona la directory dove copiare il Programma Preferenze.")
      (set #BadPrefDir          "Spiacente, ma il programma Principale e quello delle Preferenze non possono essere installati nella stessa directory !")
      (set #CopyingPrefProg     "Sto copiando il programma per le Preferenze...")
      (set #UpdateMainTT1       "Sto aggiornando il tooltype %s del programma Principale...")
      (set #UpdateMainTT2       "Non dimenticare di aggiornare il tooltype %s del programma Principale.")
      (set #UpdateMainTTH1      "Questo permetter� al programma Principale di chiamare il programma Preferenze.")
      (set #UpdateMainTTH2      "Questo permetter� al programma Principale di trovare il file di aiuto.")
      (set #UpdatePrefTT1       "Sto aggiornando il tooltype %s del programma Preferenze...")
      (set #UpdatePrefTT2       "Non dimenticare di aggiornare il tooltype %s del programma Preferenze.")
      (set #UpdatePrefTTH2      "Questo permetter� al programma Preferenze di trovare il file di aiuto.")
      (set #SelectHelpDir       "Seleziona la directory dove copiare l'aiuto in-linea.")
      (set #CopyingHelp         "Sto copiando aiuto in-linea...")
      (set #SelectXPKDir        "Seleziona la directory dove copiare librerie XPK.")
      (set #CopyingXPK          "Sto copiando librerie XPK...")
      (set #SelectCatalogDir    "Seleziona la directory dove copiare il file catalogo.")
      (set #CopyingCatalog      "Sto copiando il file catalogo...")
      (set #RunPrefProg         "Eseguo il programma per le Preferenze.")
      (set #RunPrefHelp         "Questo ti permette di impostare le Preferenze di ABackup.")
      (set #Shareware1          "ABackup � un programma SHAREWARE !")
      (set #Shareware2          "Se ti piace e lo usi, � richiesto un contributo di US $20.")
      (set #Shareware3          "Per favore, leggi la sezione 'Diffusione' della documentazione, per ulteriori dettagli")
    )
  )
))
;
;********* Install the main program *******************************************
;
(procedure InstallMainProg (
  (set MainDir
    (askdir
      (prompt #SelectMainDir)
      (help @askdir-help)
      (default "SYS:Utilities")
    )
  )
  (copyfiles
    (prompt #CopyingMainprog)
    (help @copyfiles-help)
    (source (tackon SourceDir "ABackup/ABackup"))
    (dest MainDir)
    (infos)
  )
))
;
;********* Install the Prefs program *******************************************
;
(procedure InstallPrefProg (
  (set PrefDir MainDir)
  (until (NOT (= PrefDir MainDir)) (
    (set PrefDir
      (askdir
	(prompt #SelectPrefDir)
	(help @askdir-help)
	(default DefPrefDir)
      )
    )
    (if (= PrefDir MainDir) (message #BadPrefDir))
  ))
  (copyfiles
    (prompt #CopyingPrefprog)
    (help @copyfiles-help)
    (source (tackon SourceDir "Prefs/ABackup"))
    (dest PrefDir)
    (infos)
  )
  (if (NOT (= PrefDir DefPrefDir))
    (if (BITAND ToInstall 1)
      (tooltype
	(prompt (#UpdateMainTT1 "PREFSPATH"))
	(help #UpdateMainTTH1)
	(dest (tackon MainDir "ABackup"))
	(settooltype "PREFSPATH" PrefDir)
	(confirm)
      )
      (message (cat (#UpdateMainTT2 "PREFSPATH") #UpdateMainTTH1))
    )
  )
))
;
;********* Install the help files **********************************************
;
(procedure InstallHelp (
  (set HelpDir
    (askdir
      (prompt #SelectHelpDir)
      (help @askdir-help)
      (default DefHelpDir)
    )
  )

  (if (= @language "fran�ais")
      (set SrcHelpDir "fran�ais")
      (set SrcHelpDir "english")
  )

  (copyfiles
    (prompt #CopyingHelp)
    (help @copyfiles-help)
    (source (tackon (tackon SourceDir (tackon "ABackup/Docs/" SrcHelpDir)) "ABackup.guide"))
    (dest HelpDir)
  )
  (if (NOT (= HelpDir DefHelpDir))
    (if (BITAND ToInstall 1)
      (tooltype
	(prompt (#UpdateMainTT1 "HELPPPATH"))
	(help #UpdateMainTTH2)
	(dest (tackon MainDir "ABackup"))
	(settooltype "HELPPATH" HelpDir)
	(confirm)
      )
      (message (cat (#UpdateMainTT2 "HELPPPATH") #UpdateMainTTH2))
    )
  )

  (copyfiles
    (prompt #CopyingHelp)
    (help @copyfiles-help)
    (source (tackon (tackon SourceDir (tackon "Prefs/Docs/" SrcHelpDir)) "ABackupPrefs.guide"))
    (dest HelpDir)
  )
  (if (NOT (= HelpDir DefHelpDir))
    (if (BITAND ToInstall 2)
      (tooltype
	(prompt (#UpdatePrefTT1 "HELPPPATH"))
	(help #UpdatePrefTTH2)
	(dest (tackon PrefDir "ABackup"))
	(settooltype "HELPPATH" HelpDir)
	(confirm)
      )
      (message (cat (#UpdatePrefTT2 "HELPPPATH") #UpdatePrefTTH2))
    )
  )
))
;
;********* Install XPK library ************************************************
;
(procedure InstallXPK (
  (set XPKDir
    (askdir
      (prompt #SelectXPKDir)
      (help @askdir-help)
      (default "LIBS:")
    )
  )
  (copylib
    (prompt #CopyingXPK)
    (help @copylib-help)
    (source (tackon SourceDir "libs/xpkmaster.library"))
    (dest XPKDir)
  )
  (foreach XPKSourceDir "#?.library"
    (copylib
      (prompt #CopyingXPK)
      (help @copylib-help)
      (source (tackon XPKSourceDir @each-name))
      (dest (tackon XPKDir "compressors"))
    )
  )
))
;
;********* Install the catalogs ***********************************************
;
(procedure InstallCatalog (
  (set CatalogDir
    (askdir
      (prompt #SelectCatalogDir)
      (help @askdir-help)
      (default DefCatalogDir)
    )
  )
  (copyfiles
    (prompt #CopyingCatalog)
    (help @copyfiles-help)
    (source (tackon (tackon (tackon SourceDir "ABackup/Catalogs") @language) "abackup.catalog"))
    (dest CatalogDir)
  )
  (copyfiles
    (prompt #CopyingCatalog)
    (help @copyfiles-help)
    (source (tackon (tackon (tackon SourceDir "Prefs/Catalogs") @language) "abackupprefs.catalog"))
    (dest CatalogDir)
  )
))
;
;********* Start of the installation program **********************************
;
(complete 0)
(user 2)
(set #InstallWhat "")
;
;********* Setup messages and check language
;
(SetupMessages)
(if (= #InstallWhat "")
  (
    (set langnum
      (askchoice
	(prompt "Which language do you speak ?")
	(help @askoptions-help)
	(choices "deutsch" "english" "fran�ais" "italiano")
      )
    )
    (set @language (select langnum "deutsch" "english" "fran�ais" "italiano"))
    (SetupMessages)
  )
)
;
;********* Displays the welcome message
;
(user 0)
(welcome)
;
;********* Ask which part is to be installed
;
(set ToInstall
  (askoptions
    (prompt #InstallWhat)
    (help @askoptions-help)
    (if (= #InstallChoice5 "")
	(choices #InstallChoice1 #InstallChoice2 #InstallChoice3 #InstallChoice4)
	(choices #InstallChoice1 #InstallChoice2 #InstallChoice3 #InstallChoice4 #InstallChoice5)
    )
  )
)
;
;******** Install each part
;
(complete 15)
(if (BITAND ToInstall  1) (InstallMainProg))
(complete 30)
(if (BITAND ToInstall  2) (InstallPrefProg))
(complete 45)
(if (BITAND ToInstall  4) (InstallHelp))
(complete 60)
(if (BITAND ToInstall  8) (InstallXPK))
(complete 75)
(if (BITAND ToInstall 16) (InstallCatalog))
(complete 90)
;
;******** Call the Pref program
;
(if (BITAND ToInstall  2)
  (
    (textfile
      (dest TmpFile)
      (append (cat "Run <NIL: >NIL: " (tackon PrefDir "ABackup")))
    )
    (execute
      (TmpFile)
      (prompt #RunPrefProg)
      (help #RunPrefHelp)
      (confirm)
    )
    (delete TmpFile)
  )
)
;
;******** End
;
(if (= MainDir "")
  (set @default-dest PrefDir)
  (set @default-dest MainDir)
)
(message (cat #Shareware1 "\n" #Shareware2 "\n" #Shareware3))
(complete 100)
;
