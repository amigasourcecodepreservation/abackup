@database ????
@master ????
@width 80
@width 76
@node Main "Page de Garde"
@toc Sommaire 
============================================================================
                  Programme de param�trage de ABackup 5.20

         Copyright (C) 1993-1998 par Reza ELGHAZI & Denis GOUNELLE
============================================================================

Sauf  autorisation  �crite,  toute  utilisation  commerciale  ou  vente  est
strictement interdite. Vous pouvez  copier  et  diffuser  ce  programme  aux
conditions suivantes : 

    1. L'ensemble des fichiers doit �tre fourni 
    2. Aucun fichier ne doit avoir �t� modifi� 
    3. Vous ne devez pas demander plus de 40FF pour cela 

Malgr� de nombreux tests, nous ne pouvons garantir que ABackup  ne  contient
aucune erreur. VOUS UTILISEZ CE PROGRAMME A VOS RISQUES ET PERILS.  Nous  ne
pouvons en aucun cas �tre tenus pour responsables de tout dommage, direct ou
indirect, r�sultant de l'utilisation de ABackup. 

@endnode
@node Sommaire "Sommaire"

Le programme de param�trage de ABackup fonctionne de la m�me fa�on  que  les
programmes de param�trage fournis avec le syst�me d'exploitation de l'Amiga.
Aussi, cette documentation se contente de d�crire la liste  des  param�tres,
et leur influence sur le fonctionnement de ABackup. 

Il est conseill� d'installer ce programme dans  le  r�pertoire  "SYS:Prefs",
avec les autres programmes  de  param�trage.  Si  vous  pr�f�rez  le  copier
ailleurs, n'oubliez pas de modifier de type d'outil "PREFSPATH" de  ABackup,
afin que celui-ci puisse trouver le programme de param�trage. 

    @{" Options de sauvegarde               " Link "BackupOpt"}
    @{" Options de restauration             " Link "RestoreOpt"}
    @{" Options de v�rification             " Link "VerifyOpt"}
    @{" Options de compression              " Link "CrunchOpt"}
    @{" Options du d�rouleur                " Link "TapeOpt"}
    @{" Options de l'interface              " Link "GUIOpt"}
    @{" Option pour les programmes externes " Link "ExternalOpt"}
    @{" Options diverses                    " Link "MiscOpt"}

@endnode
@node BackupOpt "Options de sauvegarde"

Sauver vers 
    Indique vers quoi effectuer la sauvegarde : 

    - un gadget cyclique permet de s�lectionner le type d'archive (unit�  de
      disque, fichier, d�rouleur de bande) 

    - deux  listes  permettent  de  s�lectionner  les  unit�s  de  disque  �
      utiliser, si le type d'archive est positionn� � "Unit�". La  liste  de
      gauche indique toutes les unit�s possibles, la liste de droite indique
      les unit�s � utiliser. Le passage d'une unit� d'une liste � l'autre se
      fait simplement en cliquant sur le nom � transf�rer. 

    - un gadget de chaine, et  un  bouton  d'appel  de  requ�te,  permettent
      d'indiquer le nom  du  fichier  archive,  si  le  type  d'archive  est
      positionn� � "Fichier". 

Taille tampon 
    Indique la taille de la m�moire tampon utilis�e par la t�che fille  pour
    les �criture asynchrones,  ainsi  que  pour  l'�criture  sur  une  bande
    magn�tique.  Une  taille  de  tampon  bien  r�gl�e  permet   d'acc�l�rer
    sensiblement la vitesse de sauvegarde. 

Fichier journal 
    Si un nom de fichier est indiqu�, une ligne de statistiques sera ajout�e
    dans ce fichier, � chaque sauvegarde. Elle indiquera la date et  l'heure
    de la sauvegarde, la source  de  la  sauvegarde,  le  nom  de  l'archive
    destination, le nombre de fichiers et la taille totale de la sauvegarde,
    ainsi que le commentaire �ventuel associ� � cette sauvegarde. 

Commentaire 
    Indique le commentaire par  d�faut  �  associer  aux  sauvegardes.  Voir
    l'option "Ajouter commentaire" ci-dessous. 

Rapport 
    Active la g�n�ration d'un rapport de sauvegarde. Ce  rapport  peut  �tre
    envoy� vers l'imprimante ou vers un fichier.  Vous  pouvez  demander  un
    rapport d�taill� ou bref. 

V�rifier 
    Active la v�rification des �critures sur disquettes (ou disques dans  le
    cas d'une sauvegarde vers  une  partition).  Cette  option  ralentit  la
    sauvegarde, mais la rend plus s�re. @{b} 
    ATTENTION ! Il  est  impossible  de  garantir  que  la  sauvegarde  sera
    correcte, m�me lorsque l'option de v�rification  est  activ�e.  Si  vous
    voulez �tre certain que l'archive ne contient pas d'erreur, utilisez  la
    fonction de v�rification d'archive de ABackup. @{ub} 

Utiliser t�che fille 
    Active  l'utilisation  d'une  t�che  s�par�e  pour  les  �critures   sur
    disquettes, ce qui permet des �critures asynchrones. Suivant  la  taille
    du tampon choisie (voir plus haut), ceci permet d'acc�l�rer  notablement
    les sauvegardes. 

Sauver les liens 
    Active la sauvegarde des liens  cr��s  par  la  commande  "MakeLink"  de
    l'AmigaDOS. 

Ajouter commentaire 
    Si cette option est activ�e, ABackup vous demandera  d'entrer  un  petit
    commentaire � la fin de chaque sauvegarde. Ce commentaire sera affich� �
    l'ouverture de l'archive, avant une restauration ou une v�rification, et
    indiqu� dans le journal de sauvegarde. 

Ajouter ic�ne 
    Active la cr�ation d'une ic�ne (fichier ".info") lors  d'une  sauvegarde
    vers un fichier. 

Compresser donn�es 
    Active la compression des donn�es sauvegard�es.  Notez  que  ABackup  ne
    compressera pas les fichiers  de  moins  de  512  octets.  De  plus,  il
    reconnait  les  fichiers  d�j�  compress�s  �  l'aide   de   LhA,   Zoo,
    PowerPacker, ou XPK, et  ne  perdra  pas  de  temps  �  essayer  de  les
    compresser � nouveau. Voir @{" Options de compression " Link "CrunchOpt"}. 

Compresser catalogue 
    Active la compression du  catalogue  de  l'archive.  Le  catalogue  sera
    compress� avec le m�me algorithme que les donn�es. 

Crypter 
    Active le cryptage des donn�es, lors de la sauvegarde, � l'aide d'un mot
    de passe indiqu� par l'utilisateur au d�but de la sauvegarde. @{b} 
    ATTENTION ! Ce mot de passe sera OBLIGATOIRE pour toute restauration  ou
    v�rification. Si vous ne vous en souvenez plus,  les  donn�es  archiv�es
    seront d�finitivement inaccessibles. Il est  INUTILE  de  contacter  les
    auteurs, pour tenter de r�cup�rer ces donn�es : il nous sera  impossible
    de vous venir en aide dans ce cas. @{ub} 

Changer bit d'archive 
    Active le positionnement du bit d'archive (voir  commande  "Protect"  de
    l'AmigaDOS) sur tous les fichiers correctement sauvegard�s. Ce bit �tant
    remis � z�ro par l'AmigaDOS, � chaque modification  d'un  fichier,  ceci
    vous permettra d'effectuer une sauvegarde  incr�mentale  (ne  comprenant
    que les fichiers modifi�s) � l'aide de la s�lection par  bits  (voir  la
    documentation de ABackup). 

Dupliquer catalogue 
    Si  cette  option  est  activ�e,  ABackup  dupliquera  le  catalogue  de
    l'archive (normalement simplement ajout� � la fin de l'archive) vers  un
    fichier. Le nom de ce fichier peut �tre  indiqu�  en  argument  lors  de
    l'appel de  ABackup,  sinon  il  vous  sera  demand�  �  la  fin  de  la
    sauvegarde. 
    Cette option est tr�s utile lors d'une sauvegarde sur bande  magn�tique,
    car elle �vite de relire toute la bande pour aller chercher le catalogue
    � la fin, puis de devoir tout rembobiner pour commencer la  restauration
    ou la v�rification. 

Ignorer SKIPME 
    Cette option contr�le le fait que ABackup  examine  le  commentaire  des
    r�pertoires. Normalement,  quand  ABackup  charge  une  arborescence  de
    r�pertoires, en vue d'une s�lection pour une sauvegarde, tout r�pertoire
    dont le commentaire  commence  par  "SKIPME"  est  ignor�.  Ceci  permet
    d'�viter de perdre du temps � charger le contenu d'un  gros  r�pertoire,
    que vous ne sauvegardez jamais. 
    Si cette option est activ�e, ABackup chargera le  contenu  de  tous  les
    r�pertoires qu'il trouve. Si elle est d�sactiv�e, seuls les  r�pertoires
    dont le commentaire ne commence pas par "SKIPME" seront charg�s. 


@endnode
@node RestoreOpt "Options de restauration"

Restaure depuis 
    Indique l'archive source, contenant les donn�es � restaurer.  Pour  plus
    de d�tails, voir l'option "Sauve vers" des @{" Options de sauvegarde " Link "BackupOpt"}. 

Restaure vers 
    Indique le r�pertoire dans lequel restaurer les fichiers. Si  ce  gadget
    est vide, les fichiers  seront  restaur�s  dans  le  r�pertoire  o�  ils
    �taient lors de la sauvegarde. 

Si existe d�j� 
    Indique � ABackup ce qu'il doit  faire  lorsqu'un  fichier  �  restaurer
    existe d�j�. Les choix possibles sont : 

    - Toujours remplacer (le fichier existant sera �cras�) 
    - Ne jamais remplacer (le fichier ne sera pas restaur�) 
    - Demander si remplacer (ABackup vous demandera si vous voulez remplacer
      le fichier) 
    - Remplacer si plus ancien (le fichier existant ne sera �cras� que  s'il
      est plus ancien que le fichier pr�sent dans l'archive) 
    - Renommer fichier (ABackup  vous  demandera  un  nouveau  nom  pour  le
      fichier � restaurer) 

Si �choue 
    Indique � ABackup ce qu'il  doit  faire  lorsque  la  restauration  d'un
    fichier �choue. En effet, dans  ce  cas,  le  fichier  sur  disque  sera
    �ventuellement incomplet, ou contiendra des donn�es erron�es. Les  choix
    possibles sont : 

    - Toujours d�truire (le fichier sera d�truit) 
    - Ne jamais d�truire (le fichier sera conserv�) 
    - Demander si d�truire (ABackup vous demandera si vous  voulez  d�truire
      le fichier) 

Rapport 
    Active le rapport de restauration. Ce  rapport  peut  �tre  envoy�  vers
    l'imprimante ou  vers  un  fichier.  Vous  pouvez  demander  un  rapport
    d�taill� ou bref. 

Arborescence 
    Active la restauration de la structure des r�pertoires, telle qu'elle  a
    �t� enregistr�e lors de la sauvegarde. Si cette option  est  d�sactiv�e,
    tous les fichiers seront restaur�s dans le m�me r�pertoire. 

Restaurer la date 
    Lorsqu'un fichier est restaur�, l'AmigaDOS lui donne comme date la  date
    � laquelle il a �t� restaur�.  Si  cette  option  est  activ�e,  ABackup
    remettra la date que le fichier avait lors de sa sauvegarde. 

Restaurer les liens 
    Si cette option est activ�e,  ABackup  restaurera  �galement  les  liens
    pr�sents dans l'archive (voir commande "MakeLink" de l'AmigaDOS). 

R�pertoires vides 
    Si cette option est activ�e, ABackup re-cr�era �galement les r�pertoires
    vides pr�sents dans l'archive. 

Fichier catalogue 
    Si cette option est activ�e, ABackup chargera le catalogue de  l'archive
    depuis un fichier. Le nom de ce fichier peut �tre indiqu�  en  argument.
    Sinon, il vous sera demand� avant le chargement du catalogue. 

@endnode
@node VerifyOpt "Options de v�rification"

Rapport 
    Active le rapport de v�rification. Ce  rapport  peut  �tre  envoy�  vers
    l'imprimante ou  vers  un  fichier.  Vous  pouvez  demander  un  rapport
    d�taill� ou bref. 

Comparer les donn�es 
    Par d�faut, ABackup se contente d'essayer  de  restaurer  les  fichiers,
    sans v�rifier que les donn�es sont correctes. Gr�ce �  l'utilisation  de
    sommes de contr�le, ceci est d�j� une v�rification relativement s�re. Si
    cette option est  activ�e,  ABackup  effectuera  une  v�rification  plus
    approfondie, en comparant les donn�es extraites de l'archive avec celles
    pr�sentes sur disque. 
    Il va de soit que cette option ralentit la vitesse de  v�rification.  De
    plus, ABackup ne pourra v�rifier que les fichiers encore pr�sents sur le
    disque, et n'ayant  pas  �t�  modifi�s.  Pour  d�tecter  une  �ventuelle
    modification, le programme compare la taille et la date des fichiers. 

Ignorer la date 
    Si cette option est activ�e, ABackup ne testera pas la date  du  fichier
    sur disque avant de le comparer avec le fichier archiv�. 

Choisir les fichiers 
    Par d�faut, ABackup v�rifie tous les fichiers archiv�s. Si cette  option
    est activ�e, vous pourrez choisir quels fichiers v�rifier, �  l'aide  de
    la fen�tre de s�lection habituelle. 

@endnode
@node CrunchOpt "Options de compression"

M�thode 
    Indique la m�thode de compression � utiliser.  Vous  pouvez  au  minimum
    choisir entre "Interne" (utilisation d'un algorithme interne �  ABackup)
    et "Externe" (utilisation d'un programme de  compression  externe,  voir
    ci-dessous).  Si  la  biblioth�que   XPK   est   install�e,   le   choix
    "Biblioth�que XPK" sera automatiquement ajout�. 
    Notez que  la  compression  externe  n'est  pas  utilisable  lors  d'une
    sauvegarde de partitions. 

M�thode XPK/Mode XPK 
    Dans le cas o� la m�thode de compression choisie est "Biblioth�que XPK",
    ces gadgets vous permettent de s�lectionner une des m�thodes de XPK,  et
    le mode pour cette m�thode. 

Compression/D�compression 
    Ces deux gadgets permettent d'indiquer  le  nom  et  les  arguments  des
    programmes externes de compression et de d�compression. Les  conventions
    suivantes peuvent �tre utilis�es pour les arguments : 

    %0 (ou %s)    : nom complet du fichier source
    %1            : chemin d'acc�s du fichier source
    %2            : nom du fichier source (sans chemin)
    %3 (ou %d)    : nom complet du fichier destination
    %4            : chemin d'acc�s du fichier destination
    %5            : nom du fichier destination (sans chemin)

    A titre d'exemple,  voici  les  valeurs  �  indiquer  pour  utiliser  le
    programme LhA (copyright par Stefan Boberg) : 

    Compression   : LhA a -X %d %s
    D�compression : LhA e -X %s %5 %4

    (Vous pouvez ne pas indiquer l'option -X,  mais  il  vous  faudra  alors
    appeler ABackup avec l'argument ECSUFFIX renseign� � ".lha"). 

    Lorsque vous utilisez la  compression  par  programme  externe,  il  est
    recommand� de rendre ce programme r�sident, ou de  le  copier  en  RAM:,
    afin de diminuer au maximum le temps de chargement de ce  programme.  En
    effet, ce programme sera  appel�  par  ABackup  pour  chaque  fichier  �
    compresser ou d�compresser, ce qui veut dire que, sur une sauvegarde  de
    1000 fichiers, un gain d'une seconde sur le temps de chargement  produit
    un gain de plus d'un quart d'heure sur la dur�e totale de la sauvegarde.

    Lorsque ABackup appelle le programme de compression ou de d�compression,
    il envoit les  messages  produits  par  ce  programme  vers  le  fichier
    "T:ABackup.log". Vous pouvez consulter ce fichier afin de voir ce qui se
    passe, en cas de probl�me. 

Filtre 
    Indique la liste des extensions de nom  de  fichiers  �  exclure  de  la
    compression. Par exemple, si la liste contient "lzx", les fichiers  dont
    le nom se termine par ".lzx" ne seront pas compress�s. 

@endnode
@node TapeOpt "Options du d�rouleur"

Pilote 
    Indique le nom du pilote SCSI � utiliser (par d�faut: "scsi.device"). 

Port SCSI 
    Indique le num�ro de port SCSI sur lequel est connect� le d�rouleur. 

Taille de bloc 
    Indique la taille de bloc support�e par le lecteur. 

Rembobiner 
    Active le rembobinage automatique de la bande, en fin d'op�ration. 

Tendre auto. 
    Si cette option est activ�, ABackup retendra la bande automatiquement en
    d�but d'op�ration. 

Ejecter 
    Active l'�jection automatique de la cartouche, en fin d'op�ration. Cette
    op�ration n'est support�e que par certains lecteurs. 

Tampon en FAST mem. 
    Demande que l'allocation de la m�moire tampon pour  les  entr�es/sorties
    soit faite en m�moire FAST, plut�t qu'en m�moire CHIP. 

Interrogation 
    Si le nom du pilote SCSI et le num�ro de port  SCSI  sont  corrects,  ce
    gadget permet d'interroger le lecteur. Les  informations  re�ues  seront
    alors affich�es pour contr�le. 

@endnode
@node GUIOpt "Options de l'interface"

Type d'�cran 
    Indique le type d'�cran � utiliser :  �cran  de  l'atelier  (Workbench),
    �cran propre � ABackup (Personnalis�) ou �cran public. 

Nom �cran public 
    Indique le nom de l'�cran public � utiliser, dans  le  cas  o�  le  type
    d'�cran choisit est "Public". 

Dans le cas o� le type d'�cran choisit est "Personnalis�", il  est  possible
de s�lectionner le mode de l'�cran � ouvrir. La requ�te de s�lection est  la
requ�te de l'asl.library, ou �  d�faut  celle  de  la  reqtools.library.  Si
aucune de ces biblioth�ques n'est disponible (une version V38 ou  sup�rieure
est n�cessaire), il sera impossible de changer la r�solution de l'�cran. 

Police de l'�cran 
    Indique la police de l'�cran personnalis�. 

Police du texte 
    Indique la  police  utilis�e  pour  l'affichage  dans  les  fen�tres  de
    ABackup, quel que soit le type d'�cran choisit. 

Palette 
    Dans le cas o� le  type  d'�cran  choisit  est  "Personnalis�",  il  est
    possible  de  changer  les  couleurs  de  cet  �cran.  La   biblioth�que
    reqtools.library est n�cessaire pour cette requ�te (V38 ou sup�rieure). 

@endnode
@node ExternalOpt "Option pour les programmes externes"

Cette fen�tre permet de param�trer la visualisation des  fichiers,  qui  est
activ�e en double-cliquant sur  un  nom  de  fichier,  dans  le  fen�tre  de
s�lection des fichiers de ABackup. 

ASCII 
    Nom du programme de visualisation des fichiers contenant du texte. 

ILBM 
    Nom du programme de visualisation des fichiers contenant une image IFF. 

Autres 
    Nom du programme de visualisation des autres fichiers. 

Lancer en arri�re plan 
    Active le lancement du programme de  visualisation  "en  arri�re  plan",
    afin de ne pas bloquer ABackup  tant  que  la  visualisation  n'est  pas
    termin�e. 

Confirmer la visualisation 
    Tant que cette option est activ�e, ABackup vous  demandera  confirmation
    avant de lancer la  visualisation  d'un  fichier.  Ceci  vous  permettra
    �ventuellement de modifier le nom du programme � ex�cuter. 

@endnode
@node MiscOpt "Options diverses"

Alerte 
    Indique comment ABackup attirera votre attention  lorsqu'une  erreur  se
    produira, o� lorsqu'il  aura  besoin  que  vous  effectuiez  une  action
    particuli�re. Les valeurs possibles sont : 

    - Aucune (ABackup se contentera d'afficher une boite de requ�te) 
    - Bip sonore (ABackup envoyera un "bip" sonore) 
    - Signal visuel (ABackup fera flasher l'�cran) 
    - Bip & signal  (ABackup  envoyera  un  "bip"  sonore  et  fera  flasher
      l'�cran) 

Taille des fichiers 
    Indique l'unit� utilis�e pour afficher  la  taille  des  fichiers  :  en
    octets, en Kilo-octets, en Mega-octets, ou automatiquement (s�lection de
    l'unit� la plus grande possible). 

R�pertoire temporaire 
    Indique le nom du r�pertoire � utiliser comme  espace  de  travail,  par
    exemple pour  cr�er  des  fichiers  de  travail.  Pour  des  raisons  de
    s�curit�, il est d�conseill� d'indiquer un r�pertoire qui se trouve  sur
    une des partitions en cours de sauvegarde. 

R�p. des s�lections 
    Indique le nom du r�pertoire contenant les fichiers de  s�lection  (voir
    la documentation de ABackup pour plus de  d�tails  sur  l'enregistrement
    des s�lections). 

Imprimer �tiquettes 
    Active l'impression automatique d'�tiquettes, en fin de sauvegarde, pour
    les disquettes de sauvegarde. 

Lignes par �tiquettes 
    Indique le nombre de lignes entre deux �tiquettes. @endbase 
@endnode
